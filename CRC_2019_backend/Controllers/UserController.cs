﻿using CRC_2019_backend.Services;
using CRC_2019_backend.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace CRC_2019_backend.Controllers
{
    [AllowAnonymous]
    [Route("api/user")]
    [ApiController]
    public class UserController : BaseCrcController
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService, IRoleService roleService)
        {
            _userService = userService;
        }

        [HttpGet("all")]
        public IEnumerable<UserViewModel> GetAllUsers()
        {
            return _userService.GetAllUsers();
        }

        [HttpGet("token/{userName}/{password}")]
        public ActionResult<string> GetToken(string userName, string password)
        {
            return ExecuteAction(() => _userService.GetToken(userName, password));
        }
    }
}
