﻿using System.Collections.Generic;
using CRC_2019_backend.ViewModels;

namespace CRC_2019_backend.Services
{
    public interface IRequestService
    {
        int CreateNewRequest(CreateRequestViewModel request);

        IEnumerable<ReadRequestViewModel> GetAllRequests();

        IEnumerable<ReadRequestViewModel> GetAllRequestsForUser(int userId);

        void Approve(int requestId);

        void Reject(int requestId);

        void Delete(int id);
    }
}